# TODO

Remove HTML tag attributes

	/<html[^>]*>/

Remove typekit classes (these state that a webfont has been loaded)

	/wf-[A-z0-9-]+-active/g

WordFence live traffic

	/\?wordfence_lh=1&hid=[A-z0-9]*/g

GA noscript (regex todo)

	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MTDG7CZ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
