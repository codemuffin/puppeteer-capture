/**
 * Capture a bulk lot of sites
 *
 * node batch <path_to_batch_json_file>
 * node batch "./json/batch/multidemo.json"
 */

// Modules
// ============================================================================

// Utils
const readFile      = require('./modules/utils/readFile');
const runScript     = require('./modules/utils/runScript');
const getDateString = require('./modules/utils/getDateString');
const logStatus     = require('./modules/utils/logStatus');


// Functions
// ============================================================================

/**
 * Init batch capture
 *
 * @param   {String}  sitesJsonPath  Path to a JSON containing an array of site config JSONs
 *
 * @return  {void}
 */
async function init( sitesJsonPath )
{
	let sitesListArr = [];

	try
	{
		const sitesListJson = await readFile( sitesJsonPath );
		sitesListArr = JSON.parse( sitesListJson );

		if ( !sitesListArr.length )
		{
			throw new Error( 'Sites array JSON was valid but empty' );
		}
	}
	catch( err )
	{
		throw new Error( err );
	}

	const scriptPath = './index.js';
	const outputParentDir = '\\batch-' + getDateString();

	for( let site of sitesListArr )
	{
		try
		{
			await runScript( scriptPath, [ site, false, outputParentDir ] );
		}
		catch( err )
		{
			console.log( 'Error for site: ' + site );
			throw new Error(  err );
		}
	}

	logStatus( 'success', 'Batch finished' );
}


// CLI Init
// ============================================================================

try
{
	const defaultSitesJsonPath = './json/batch/quick.json';

	const ARGS = {
		// Arg 1: Path to sites list JSON
		sitesJsonPath: ( process.argv[2] ) ? process.argv[2] : defaultSitesJsonPath
	};

	if ( ARGS.sitesJsonPath )
	{
		init( ARGS.sitesJsonPath );
	}
	else
	{
		console.log( 'Sites JSON path must be specified as the first argument' );
	}
}
catch( err )
{
	console.log( 'BATCH ERROR' );

	if ( err )
	{
		console.log( err );
	}
}
