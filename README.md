# Puppeteer Capture

Saves a site's markup and rendered DOM, with screenshots for mobile/tablet/desktop.

Accepts a single JSON file for one site, or a batch JSON file pointing to multiple single JSON files.


## Usage

Install:

	npm i

Run on a single JSON:

	node index <path_to_single_json> [<timestamp_dir> <output_parent_dir>]

	node index "./json/live/theverge.com.json"

Run a batch:

	node batch "./json/batch/multidemo.json"

Batch JSON files should contain an array of JSON files, with paths relative to the project root.

## Todo

### Bugs

* [ ] Allow trailing slashes on urls (done?)
* [ ] Fix the request block issue (possible? research suggests bug in puppeteer)
* [ ] Check URLs using query strings, they seem to hang

### General

* [ ] Apply filters separately (rather than during the capture process)
* [ ] Capture pages & concurrently (currently sequential)

### Nice2have

* [ ] Write request/response log to file
* [ ] Report times: Start + end
* [ ] Report counts: Success + failure/error
* [ ] Validate JSON configs
* [ ] Electron frontend wi. JSON builder
* [ ] Edit config via JSON (it's currently set in _core/setupAppConfig.js_)

### Done

* [x] Bulk captures, using a JSON listing other JSON files

## Screenshots

Running:

![](_docs/running.png)

Output files:

![](_docs/output-files.png)

Batch captures are grouped into timestamped folders for easy batch diffs:

![](_docs/batch-folders.png)

Check deploys by diffing before/after captures:

![](_docs/winmerge.png)
