/**
 * Capture a web page. Saves markup and a screenshot
 *
 * node index <path_to_single_json> [<timestamp_dir> <output_parent_dir>]
 * node index "./json/example.json"
 */

// Modules
// ============================================================================

// Dependances
const path           = require('path');

// Unique
const setupAppConfig = require('./modules/core/setupAppConfig');
const capture        = require('./modules/core/capture');

// Utils
const logStatus      = require('./modules/utils/logStatus');
const readFile       = require('./modules/utils/readFile');


// Functions
// ============================================================================

async function init( ARGS )
{
	const APPCONFIG = setupAppConfig( __dirname, ARGS );

	try
	{
		// Site config
		const configFileData = await readFile( APPCONFIG.paths.siteConfigPath );
		const SITECONFIG = JSON.parse( configFileData );

		logStatus( 'important', SITECONFIG.name );

		// Core functionality
		await capture( APPCONFIG, SITECONFIG, APPCONFIG );

		logStatus( 'success', 'Capture complete' );
	}
	catch( err )
	{
		logStatus( 'error', 'FAILURE' );
		logStatus( 'error', 'Site config: ' + APPCONFIG.paths.siteConfigPath );

		if ( err )
		{
			logStatus( 'error', err.stack );
		}
		else
		{
			logStatus( 'warn', 'No error object was available to log' );
		}

		process.exit();
	}
}

// CLI Init
// ============================================================================

if ( require.main === module )
{
	// Called directly from the command line

	const defaultSiteConfigPath = path.resolve( __dirname + '/json/quick/skelwith-live-quick.json' );

	const ARGS = {
		// Arg 1: Site config path (to JSON file)
		siteConfigPath:   ( process.argv[2] ) ? process.argv[2] : defaultSiteConfigPath,
		// Arg 2: Timestamp single site directories?
		timestampSiteDir: ( process.argv[3] ) ? process.argv[3] : true,
		// Arg 3: Parent output directory name
		outputDirParent:  ( process.argv[4] ) ? process.argv[4] : '',
	};

	init( ARGS );
}
else
{
	// Required as a module. Not currently supported
}


// Exports
// ============================================================================

module.exports = init;
