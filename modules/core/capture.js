
// Modules
// ============================================================================

// Dependances
const puppeteer           = require('puppeteer');

// Unique
const captureLoop         = require('./captureLoop');
const getBlockedHostsArr  = require('./getBlockedHostsArr');
const responseInterceptor = require('./responseInterceptor');
const generateOutputDir   = require('./generateOutputDir');

// Utils
const logStatus           = require('../utils/logStatus');


// Functions
// ============================================================================

async function capture( APPCONFIG, SITECONFIG )
{
	const { urls, options } = SITECONFIG;
	const outputDir = generateOutputDir( APPCONFIG, SITECONFIG );

	logStatus( 'misc', 'Starting capture' );
	logStatus( 'misc', 'Output directory: ' + outputDir );

	const browser = await puppeteer.launch();
	const page = await browser.newPage();

	if ( options.requestInterception )
	{
		// Prepare the adblock array, if needed.
		// Uses the per-site option to block ad hosts.
		// This can reduce DOM bloat and inconsisties in successive renders
		let blockedArr = ( options.blockHosts ) ? await getBlockedHostsArr( APPCONFIG ) : [];

		// Request interception can block certain requests, including images
		// This is a page event listener, so must come before page.goto
		await page.setRequestInterception( true );
		page.on( 'request', async interceptedRequest => responseInterceptor( interceptedRequest, options, blockedArr ) );
	}

	// Graceful handling of response errors
	const { responseFail } = APPCONFIG.errors;
	let responseFailCount = 0;

	// Limit number of URL scrapes for thisthe given site
	// APPCONFIG.maxUrls

	// URL Loop
	for ( let url of urls )
	{
		try
		{
			await captureLoop( page, url, outputDir, APPCONFIG, SITECONFIG );
		}
		catch( err )
		{
			//#TODO: Move to separate function
			if ( responseFail.allow )
			{
				if ( responseFail.limit === -1 || responseFail.limit === "-1" )
				{
					logStatus( 'error', err );
					logStatus( 'warn', 'URL response failed, but batch will continu until complete.' );
				}
				else
				{
					responseFailCount ++;

					if ( responseFail.limit > responseFailCount )
					{
						logStatus( 'error', err );
						logStatus( 'warn', `URL response failed, but batch will continue. ${responseFailCount} out of ${responseFail.limit} allowed failures` );
					}
					else
					{
						logStatus( 'error', err );
						logStatus( 'error', 'URL response failed, and max allowed failures has been reached' );
						throw new Error( err );
					}
				}
			}
			else
			{
				throw new Error( err );
			}
		}
	};

	logStatus( 'misc', 'Close browser' );

	await browser.close();
}


// Exports
// ============================================================================

module.exports = capture;
