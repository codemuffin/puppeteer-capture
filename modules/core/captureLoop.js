
// Modules
// ============================================================================

// Unique
const filterSrc            = require('./filterSrc');
const captureScreenshots   = require('./captureScreenshots');
const generateSubDirectory = require('./generateSubDirectory');

// Utils
const logStatus            = require('../utils/logStatus');
const removeTrailingSlash  = require('../utils/removeTrailingSlash');
const sleep                = require('../utils/sleep');
const saveToFile           = require('../utils/saveToFile');


// Functions
// ============================================================================

/**
 * Runs once per URL
 */
async function captureLoop( page, url, outputDir, APPCONFIG, SITECONFIG )
{
	return new Promise( async ( resolve, reject ) =>
	{
		try
		{
			logStatus( 'misc', 'URL: ' + url );

			const { mainUrl, options } = SITECONFIG;
			const { capture, removeElements } = options;

			// Trailing slashes are troublesome!
			url = removeTrailingSlash( url );

			// Each internal page gets its own folder
			const subDir = generateSubDirectory( url, mainUrl, outputDir );

			// Change URL. Also captures the response
			const response = await page.goto( url, {
				waitUntil: 'networkidle0',
				timeout: 60000
			});

			// Data capture options
			// const { sourceMarkup, renderedDom, screenshots } = capture;

			if ( !response )
			{
				reject( new Error( 'Response was null (reason unknown)' ) );
			}

			// Check status
			if ( response.status() !== 200 )
			{
				reject( new Error( 'Response status should be 200 ("OK"). Response code: ' + response.status() ) );
			}

			// Save source
			//#TODO: Add null check for `response`
			if ( capture.sourceMarkup )
			{
				const pageResponseText = await response.text();
				await saveToFile( pageResponseText, subDir + 'source-markup.html' );
			}

			// Capture rendered DOM
			if ( capture.renderedDom  )
			{
				// Wait for DOM to render
				await sleep( APPCONFIG.wait.dom );

				let dom = await page.content();

				// Apply filters
				dom = await filterSrc( dom, options );

				await saveToFile( dom, subDir + 'rendered-dom.html' );
			}

			// Capture screenshots
			if ( capture.screenshots )
			{
				await captureScreenshots( page, subDir, APPCONFIG, removeElements );
			}

			resolve();
		}
		catch ( err )
		{
			reject( err );
		}
	});
}


// Exports
// ============================================================================

module.exports = captureLoop;
