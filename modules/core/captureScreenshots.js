
// Modules
// ============================================================================

// Utils
const logStatus = require('../utils/logStatus');
const sleep     = require('../utils/sleep');


// Functions
// ============================================================================

async function captureScreenshots( page, subDir, APPCONFIG, removeElements = false )
{
	const { viewports, wait } = APPCONFIG;

	// Remove elements
	if ( removeElements && removeElements.length )
	{
		for ( let element of removeElements )
		{
			logStatus( 'misc', `Remove element "${element}"` );

			await page.evaluate( selector =>
			{
				const elements = document.querySelectorAll( selector );

				elements.forEach( el =>
				{
					el.parentNode.removeChild( el );
				});
			}, element );
		};
	}

	for ( const viewport of Object.keys( viewports ) )
	{
		logStatus( 'misc', 'Screenshot: ' + viewport );

		await page.setViewport( viewports[viewport] );

		await sleep( wait.screenshot );

		await page.screenshot({
			path: subDir + `screenshot-${viewport}.png`,
			fullPage: true
		});
	}
}


// Exports
// ============================================================================

module.exports = captureScreenshots;
