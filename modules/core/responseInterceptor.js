/**
 * Lets us block requests
 *
 * We could also log or alter them
 */

// Modules
// ============================================================================

// Utils
const logStatus = require('../utils/logStatus');


// Functions
// ============================================================================

async function responseInterceptor( interceptedRequest, options, blockedArr )
{
	const { blockImages, blockHosts } = options;
	const requestUrl = interceptedRequest.url();
	const requestDomain = requestUrl.split( '/' )[2];

	if ( !requestUrl || requestUrl === '' )
	{
		interceptedRequest.continue();
	}

	if ( blockImages && ( requestUrl.endsWith( '.png' ) || requestUrl.endsWith( '.jpg' ) || requestUrl.endsWith( '.jpeg' ) ))
	{
		interceptedRequest.abort();
	}
	else
	{
		if ( blockHosts && blockedArr.length )
		{
			//#TODO: Ignore first-party URLs
			const isBlocked = blockedArr.includes( requestDomain ) ? true : false;

			if ( isBlocked )
			{
				logStatus( 'misc', 'Blocked: ' + requestDomain );
				interceptedRequest.abort();
			}
			else
			{
				interceptedRequest.continue();
			}
		}
		else
		{
			interceptedRequest.continue();
		}
	}
}


// Exports
// ============================================================================

module.exports = responseInterceptor;
