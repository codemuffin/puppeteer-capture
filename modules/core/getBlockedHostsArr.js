

// Modules
// ============================================================================

// Utils
const readFile = require('../utils/readFile');


// Functions
// ============================================================================

/**
 * Read blocked-hosts file, and convert to array
 *
 * @return  {Array}  Arrray of blocked hosts
 */
async function getBlockedHostsArr( APPCONFIG )
{
	const { paths } = APPCONFIG;

	// Full ad hosts file via: http://winhelp2002.mvps.org/hosts.txt
	const useFullHosts = false;
	const hostFilename = paths.appConfigDir + ( useFullHosts ? '\\blocked-hosts.txt' : '\\blocked-hosts-full.txt' );

	return new Promise( async ( resolve, reject ) =>
	{
		try
		{
			const blockedStr = await readFile( hostFilename );
			const blockedArr = blockedStr.split( '\r\n' );

			resolve( blockedArr );
		}
		catch( err )
		{
			reject( err );
		}
	});
}


// Exports
// ============================================================================

module.exports = getBlockedHostsArr;
