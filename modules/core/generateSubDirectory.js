/**
 * #TODO: Sanitise query strings, and any other illegal filename characters
 */


// Modules
// ============================================================================

// Utils
const logStatus           = require('../utils/logStatus');
const removeTrailingSlash = require('../utils/removeTrailingSlash');


// Functions
// ============================================================================

function generateSubDirectory( url, mainUrl, outputDir )
{
	mainUrl = removeTrailingSlash( mainUrl );

	let subDir = '';

	// Home vs. internal
	if ( url === mainUrl )
	{
		subDir = 'home';

		// Log the homepage
		logStatus( 'info', subDir );
	}
	else
	{
		// Remove main URL from internal page URL
		subDir = url.replace( mainUrl, '' );

		// Remove leading slash
		subDir = subDir.substring( 1 );

		// Log the current internal page
		logStatus( 'info', subDir );

		// Handle internal page's subdirectories
		subDir = subDir.replace( '/', '--' );
	}

	// Append with date string
	subDir = subDir;

	return outputDir + '\\' + subDir + '\\';
}


// Exports
// ============================================================================

module.exports = generateSubDirectory;
