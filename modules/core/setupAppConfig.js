
// Functions
// ============================================================================

function setupAppConfig( BASEDIR, ARGS )
{
	const { timestampSiteDir, outputDirParent } = ARGS;

	const APPCONFIG = {
		paths: {
			// Directory to save HTML and screenshots
			outputDir: BASEDIR + '\\output' + outputDirParent,

			// Global config data, e.g. hosts block file
			appConfigDir: BASEDIR + '\\config',

			// Get JSON site config path from command line arg. Default: FM demo
			siteConfigPath: ARGS.siteConfigPath,
		},
		timestampSiteDir,
		viewports: {
			mobile:  { width:  320, height:  480 },
			tablet:  { width:  768, height: 1024 },
			desktop: { width: 1920, height: 1080 },
		},
		// Allow response errors to fail without throwing an error.
		// Prevents single response errors from spoiling a batch.
		// Warning: This will fill your log with notices if all URLs are bad
		errors: {
			responseFail: {
				allow: true,
				limit: 20
			}
		},
		wait: {
			// dom: 5000,
			// screenshot: 2000
			dom: 1000,
			screenshot: 1000
		},
		filenames: {
			markup: "source-markup.html",
			dom: "rendered-dom.html"
		},

		// Required to block hosts and/or images, but can fail for unknown reasons
		requestInterception: false,

		// The number of URLs to scrape for a given site. Use -1 for all
		maxUrls: -1,
	};

	return APPCONFIG;
}


// Exports
// ============================================================================

module.exports = setupAppConfig;
