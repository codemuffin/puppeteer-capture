
// Modules
// ============================================================================

// Dependancies
const pretty        = require('pretty');

// Filters
const wpFilters     = require('../filters/wpFilters');
const googleFilters = require('../filters/googleFilters');

// Utils
const regexReplace  = require('../utils/regexReplace');
const logStatus     = require('../utils/logStatus');


// Functions
// ============================================================================

//#TODO: Push each regex repalce into an array, then loop through it using regexReplace
async function filterSrc( src, options )
{
	return new Promise( async ( resolve, reject ) =>
	{
		try
		{
			logStatus( 'misc', 'Apply filters' );

			const { wordpress, prettyHtml, stripQueryStrings, google } = options.filters;

			// Updated with replacement string parts
			let newStr = '';

			// Strip query strings
			if ( stripQueryStrings )
			{
				logStatus( 'misc', 'Filter: strip query strings' );
				newStr = 'QS_PLACEHOLDER';
				src = await regexReplace( src, /(src="[^\?\"]+\?)([^\"]+)(")/g, `$1${newStr}$3` );
			}

			// Prettify HTML
			if ( prettyHtml )
			{
				logStatus( 'misc', 'Filter: prettify html' );
				src = pretty( src );
			}

			// WP filters
			if ( wordpress )
			{
				logStatus( 'misc', 'Filter: wordpress' );
				src = await wpFilters( src );
			}

			if ( google )
			{
				// Google
				logStatus( 'misc', 'Filter: google' );
				src = await googleFilters( src );
			}

			resolve( src );
		}
		catch( err )
		{
			reject( err );
		}
	});
}


// Exports
// ============================================================================

module.exports = filterSrc;
