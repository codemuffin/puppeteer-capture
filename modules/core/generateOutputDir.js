
// Modules
// ============================================================================

// Dependances
const path          = require('path');

// Utils
const getDateString = require('../utils/getDateString');


// Functions
// ============================================================================

function generateOutputDir( APPCONFIG, SITECONFIG )
{
	const { timestampSiteDir, paths } = APPCONFIG;
	let { dirName } = SITECONFIG;

	//#TODO: fix hack
	const uniqueDirName = dirName + ( timestampSiteDir !== 'false' ? '--' + getDateString() : '' );

	// Set output directory
	const outputDir = path.resolve( paths.outputDir + `\\${uniqueDirName}\\` );

	return outputDir;
}


// Exports
// ============================================================================

module.exports = generateOutputDir;
