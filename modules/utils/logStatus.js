
// Modules
// ============================================================================

// Dependances
const chalk = require('chalk');


// Functions
// ============================================================================

function logStatus( type, msg )
{
	if ( !msg )
	{
		console.log( chalk.red( 'Expected message as a second arg in logStatus, but no message was supplied' ) );
		throw new Error( 'Bug in your code. Check logStatus args' );
	}

	let color = 'blue';

	switch( type )
	{
		case 'info':
			color = 'blue';
			break;

		case 'warn':
			color = 'yellow';
			break;

		case 'success':
			color = 'green';
			break;

		case 'error':
			color = 'red';
			break;

		// Important information
		case 'important':
			color = 'cyan';
			break;

		// Developer use (debugging)
		case 'dev':
			color = 'magenta';
			break;

		// General notes (verbose)
		case 'misc':
			color = 'gray';
			break;

		default:
			color = 'gray';
	}

	/*
	let outputMsg = '';

	if ( type === 'warn' )
	{
		outputMsg = chalk.keyword( 'orange' )( msg );
	}
	else
	{
		outputMsg = chalk[color]( msg );
	}

	console.log( outputMsg );
	*/

	console.log( chalk[color]( msg ) );
}


// Exports
// ============================================================================

module.exports = logStatus;
