
// Functions
// ============================================================================

function removeTrailingSlash( str )
{
	return ( str.charAt( str.length-1 ) === '/' ) ? str.substring( 0, str.length - 1 ) : str;
}


// Exports
// ============================================================================

module.exports = removeTrailingSlash;
