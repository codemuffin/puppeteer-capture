
// Modules
// ============================================================================

// Utils
const logStatus = require('../utils/logStatus');


// Functions
// ============================================================================

/**
 * Wait N milliseconds, then returns a resolved promise
 *
 * @param   {number}  ms  Duration to wait, in ms
 * @return  {Promise}     Resolved promise, after the given ms
 */
async function sleep( ms )
{
	logStatus( 'misc', `Waiting ${ms/1000} seconds` );

	return new Promise( resolve => setTimeout(() => resolve(), ms) );
}


// Exports
// ============================================================================

module.exports = sleep;
