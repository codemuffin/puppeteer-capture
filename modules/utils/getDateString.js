
// Functions
// ============================================================================

/**
 * Returns a date string with legal characters.
 * Used for directory timestamps
 *
 * @return  {string}  Timestamp with the format YYYY-MM-DD--H.M.S
 */
function getDateString()
{
	const timestamp = new Date(),
		year = timestamp.getFullYear();

	let month = timestamp.getMonth() + 1,
		day = timestamp.getDate(),
		hrs = timestamp.getHours(),
		min = timestamp.getMinutes(),
		sec = timestamp.getSeconds();

	// Add leading 0 where necessary
	month = ( month.toString().length === 1 ) ? '0' + month.toString() : month;
	day = ( day.toString().length === 1 ) ? '0' + day.toString() : day;
	hrs = ( hrs.toString().length === 1 ) ? '0' + hrs.toString() : hrs;
	min = ( min.toString().length === 1 ) ? '0' + min.toString() : min;
	sec = ( sec.toString().length === 1 ) ? '0' + sec.toString() : sec;

	// Example: 2019.6.10-18.30.15
	const formatted = `${year}-${month}-${day}--${hrs}.${min}.${sec}`;

	return formatted;
}


// Exports
// ============================================================================

module.exports = getDateString;
