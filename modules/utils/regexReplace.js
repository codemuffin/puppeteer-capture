
// Functions
// ============================================================================

/**
 * Regex replace that returns a promise
 *
 * @param   {string}  str          Input
 * @param   {regex}   pattern      Regex literal
 * @param   {string}  replacement  Replacement string. Use $1, $2 to insert capture groups
 *
 * @return  {Promise}               Resolves with a promise continaing the updated string. On error, rejected promise containing an error object
 */
async function regexReplace( str, pattern, replacement )
{
	return new Promise( ( resolve, reject )  =>
	{
		try
		{
			const regex = new RegExp( pattern );

			if ( regex.test( str ) )
			{
				str = str.replace( regex, replacement );
			}

			resolve( str );
		}
		catch( err )
		{
			reject( err );
		}

		resolve( str );
	});
}


// Exports
// ============================================================================

module.exports = regexReplace;
