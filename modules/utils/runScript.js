
// Modules
// ============================================================================

// Dependancies
const childProcess = require('child_process');


// Functions
// ============================================================================

/**
 * Run a Node script as a forked child process
 *
 * @param   {String}  script  Script path. Relative or absolute
 * @param   {Array}   args    Array of arguments to pass to the script
 *
 * @return  {Promise}         Resolved promise if the process exits without code 0. Rejected promise on exit 0 or error
 */
async function runScript( scriptPath, args )
{
	return new Promise( ( resolve, reject ) =>
	{
	    const process = childProcess.fork( scriptPath, args );

		process.on( 'error', err => reject( err ) );

	    process.on( 'exit', code =>
	    {
	    	if ( code !== 0 )
	    	{
	    		reject( 'Exited with code ' + code );
	    	}

	    	resolve();
	    });
	});
}


// Exports
// ============================================================================

module.exports = runScript;
