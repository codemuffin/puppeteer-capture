
// Modules
// ============================================================================

// Dependances
const fs = require('fs');

// Utils
const logStatus = require('./logStatus');


// Functions
// ============================================================================

/**
 * Read a site's JSON config file to return a JS object of parsed data
 *
 * @param   {string}  filePath  Path to the JSON config file
 * @return  {object}            Parsed JSON data object
 */
async function readFile( filePath )
{
	return new Promise( ( resolve, reject ) =>
	{
		try
		{
			fs.readFile( filePath, 'utf8', ( err, data ) =>
			{
				if ( err )
				{
					logStatus( 'error', err );
					reject( 'Error reading file' );
				}

				logStatus( 'misc', 'ReadFile OK: ' + filePath );
				resolve( data );
			});
		}
		catch( err )
		{
			logStatus( 'error', err );
			reject( 'Error reading file' );
		}
	});
}


// Exports
// ============================================================================

module.exports = readFile;
