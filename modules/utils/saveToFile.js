
// Modules
// ============================================================================

// Dependances
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');

// Utils
const logStatus = require('./logStatus');


// Functions
// ============================================================================

async function saveToFile( data, filePath )
{
	return new Promise( ( resolve, reject ) =>
	{
		// Ensure directory path exists
		mkdirp( path.dirname( filePath ), err =>
		{
			if ( err )
			{
				logStatus( 'error', err );
				reject( 'Error writing to file' );
			}

			// Actual file write
			fs.writeFile( filePath, data, err =>
			{
				if ( err )
				{
					logStatus( 'error', err );
					reject( 'Error writing to file' );
				}

				// logStatus( 'misc', 'WriteFile OK: ' + filePath );
				logStatus( 'misc', 'WriteFile OK' );
				resolve();
			});
		});
	});
}


// Exports
// ============================================================================

module.exports = saveToFile;
