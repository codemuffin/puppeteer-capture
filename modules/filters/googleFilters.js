
// Modules
// ============================================================================

// Utils
const regexReplace = require('../utils/regexReplace');


// Functions
// ============================================================================

async function googleFilters( src )
{
	return new Promise( async ( resolve, reject ) =>
	{
		let newStr = '';

		try
		{
			// Google maps
			// Eg: animation-name: _gm6616;
			newStr = 'GMAPS_UNIQUE_ID';
			src = await regexReplace( src, /(animation-name: _gm)([0-9])+(;)/g, `$1${newStr}$3` );

			// Google maps tiles. Tend to get added to DOM in different orders
			// Eg: <div style="position: absolute; left: 256px; top: 0px; width: 256px; height: 256px; transition: opacity 200ms linear 0s;"><img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i10!2i505!3i326!4i256!2m3!1e0!2sm!3i469179385!3m14!2sen-US!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!12m4!1e26!2m2!1sstyles!2zcy50OjJ8cC5jOiNmZkEyQTk4RCxzLnQ6NnxwLmM6I2ZmNUM3NjdCLHMudDo2fHMuZTpsLnQuZnxwLnc6MC4xfHAudjpvbnxwLnM6MXxwLmM6I2ZmRkZGRkZGLHMudDoyfHMuZTpsLnQuZnxwLmM6I2ZmMDAwMDAwLHMudDoyfHMuZTpsLnQuc3xwLmM6I2ZmRkZGRkZGfHAubDo1NixzLnQ6NXxzLmU6bHxwLnY6b258cC5jOiNGRkYscy50OjV8cC5jOiNmZkEyQTk4RCxzLnQ6M3xwLnY6b24!4e0&amp;key=AIzaSyDD2Rvi5TXgAQ77gfcowDShPPGmkVxRJ88&amp;token=129912"

			// Google recaptcha, part of an iframe. Hard to get an exact match without a crazy long regex pattern, so just change a close match
			// Eg: name="a-la14r5ts2qos" || a-oe525oeglfzx
			// Eg: <iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LfHkpUUAAAAALeTfpkGQ48zPazsp8Uw4ro7Zl-K&amp;co=aHR0cHM6Ly93d3cubW9zc3dvb2QuY28udWs6NDQz&amp;hl=en&amp;v=v1559543665173&amp;size=normal&amp;cb=ydxndhszvthb" width="304" height="78" role="presentation" name="a-oe525oeglfzx" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>
			newStr = 'RECAPTCHA_UNIQUE_1';
			src = await regexReplace( src, /(role="presentation" name="a-)([A-z0-9]+)(" frameborder="0" scrolling="no" sandbox="allow-forms)/g, `$1${newStr}$3` );

			// Google reCaptcha, again
			// Eg: <div style="z-index: 2000000000; position: relative;"><iframe title="recaptcha challenge" src="https://www.google.com/recaptcha/api2/bframe?QS_PLACEHOLDER" name="c-la14r5ts2qos" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox" style="width: 100%; height: 100%;"></iframe></div>
			newStr = 'RECAPTCHA_UNIQUE_2';
			src = await regexReplace( src, /(role="presentation" name="c-)([A-z0-9]+)(" frameborder="0" scrolling="no" sandbox="allow-forms)/g, `$1${newStr}$3` );

			resolve( src );
		}
		catch( err )
		{
			reject( err );
		}
	});
}


// Exports
// ============================================================================

module.exports = googleFilters;
