
// Functions
// ============================================================================

//#TODO: Use regexReeplace, after updating regexReplace to report on number of replacements made (if any)
async function wpFilters( src )
{
	return new Promise( ( resolve, reject ) =>
	{
		try
		{
			// WordFence LiveTraffic query string
			// Eg: ?wordfence_lh=1&hid=8F73644FBF7EE1268B54A4948A60099C
			// Eg: ?wordfence_lh=1&hid=814A6CE70C3A6041028ECBDA77203F30
			// Eg: '//calrec.com/?wordfence_lh=1&hid=869529D97047FD62EC8CDDA6A6C6AABC'
			// Eg: '//www.blackboxsecurity.co.uk/?wordfence_lh=1&hid=5DD4BFFB260A15ADCDB445C17548A489'
			src = src.replace( /\?wordfence_lh=1&hid=[A-z0-9]*/g, '\?wordfence_lh=1&hid=?X' );

			// TypeKit html classes
			src = src.replace( /wf-[A-z0-9-]+-active/g, 'wf-TYPEKITFONT-active' );

			// CF7 Honeypot
			// Eg: <span id="wpcf7-5d02bfa8575b4" class="wpcf7-form-control-wrap formdetails-9868i6-wrap" ><label  class="hp-message">
			const honeypotStr = 'wpcf7-honeypot';
			const honepyotNew = `$1${honeypotStr}$3${honeypotStr}$5`;
			src = src.replace( /<span id="wpcf7-[A-z0-9]+" class="wpcf7-form-control-wrap formdetails-[A-z0-9]+-wrap" ><label  class="hp-message">/g, honepyotNew );

			// Encoded mailto
			// Eg: @ = &#64;
			// Eg: mailto:inf&#111;&#64;t&#104;ef&#111;r&#101;s&#116;side&#46;c&#111;&#109;
			const encodedEmailStr = 'encoded-email';
			const encodedEmailNew = `$1${encodedEmailStr}$2}`; // only 2 capture groups (others are non-capture groups)
			src = src.replace( /(href="mailto:)(([A-z0-9])*(&#[0-9]+;)+([A-z0-9])*)+(")/g, encodedEmailNew );

			resolve( src );
		}
		catch( err )
		{
			reject( err );
		}
	});
}


// Exports
// ============================================================================

module.exports = wpFilters;
